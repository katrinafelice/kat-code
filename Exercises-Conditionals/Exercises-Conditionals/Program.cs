﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercises_Conditionals
{
    class Program
    {
        static void Main(string[] args)
        {
            /* 1 - Write a program and ask the user to enter a number.  The number should be between 
             * 1 to 10.  If the user enters a valid number, display "Valid" on the console.  Otherwise
             * display "Invalid". (This logic is used a lot in applications where values entered into
             * input boxes need to be validated.)
            */

            Console.WriteLine("Enter a number between 1 to 10.");
            var input = Convert.ToInt32(Console.ReadLine());

            if (input >= 1 && input <= 10)
            {
                Console.WriteLine("Valid");
            }
            else
            {
                Console.WriteLine("Invalid");
            }

            /* 2 - Write a program which takes two numbers from the console and 
             * displays the maximum of the two.
             */

            Console.WriteLine("Enter a number: ");
            var input1 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter another number: ");
            var input2 = Convert.ToInt32(Console.ReadLine());

            var maximum = (input1 > input2) ? input1 : input2;;

            Console.WriteLine("The maximum of the two numbers you've entered is {0}.", maximum);

            /* 3 - Write a program and ask the user to enter the width and height of an image. 
             * Then tell if the image is landscape or portrait.
             */

            Console.WriteLine("Enter the width of the image: ");
            var width = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter the height of the image: ");
            var height = Convert.ToInt32(Console.ReadLine());

            if (width < height)
            {
                Console.WriteLine("The image is portrait.");
            }

            else
            {
                Console.WriteLine("The image is landscape.");

            /* 4 - Your job is to write a program for a speed camera. For simplicity, ignore the details such as 
             * camera, sensors, etc and focus purely on the logic. Write a program that asks the user to 
             * enter the speed limit. Once set, the program asks for the speed of a car. If the user enters 
             * a value less than the speed limit, program should display Ok on the console. If the value 
             * is above the speed limit, the program should calculate the number of demerit points. 
             * For every 5km/hr above the speed limit, 1 demerit points should be incurred and displayed on 
             * the console. If the number of demerit points is above 12, the program should display 
             * License Suspended.
            */


            Console.WriteLine("What is the speed limit?");
            var speedLimit = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("What is the speed of the car?");
            var speedOfCar = Convert.ToInt32(Console.ReadLine());

            if (speedOfCar < speedLimit)
            {
                Console.WriteLine("Ok");
            }
            else
            {
                var demeritPoints = (speedOfCar - speedLimit)/5 ;
                if (demeritPoints < 12)
                {
                    Console.WriteLine("Speed limit has been exceeded, {0} demerit points have been incurred.", demeritPoints);
                }
                else
                {
                    Console.WriteLine("License Suspended.");
                }
            }


        }
    }
    
}
