﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForEachLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            //Using a for loop to display each character in a string

            var name = "John Smith";

            for (var i = 0; i < name.Length; i++)
                // var i = 0 because string is zero indexed
            {
                Console.WriteLine(name[i]);
            }

            Console.WriteLine();

            //A cleaner way to display each character in a string is to use a foreach loop
            //Whenever we have an enumerable object like a string, array, list, it's much easier to iterate
            //over it using a foreach loop

            foreach (var character in name)
            {
                Console.WriteLine(character);
            }

            Console.WriteLine();

            //An iteration over an array using a foreach loop

            var numbers = new int[] { 1, 2, 3, 4 };

            foreach (var number in numbers)
            {
                Console.WriteLine(number);
            }

        }
    }
}
