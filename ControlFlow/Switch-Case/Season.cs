﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Switch_Case
{
    //declare an enum for use in Switch-Case/Program.cs

    public enum Season
    {
        Spring,
        Summer,
        Autumn,
        Winter
    }
}
