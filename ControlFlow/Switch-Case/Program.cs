﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Switch_Case
{

    class Program
    {
        static void Main(string[] args)
        {
            //SWITCH-CASE:
            //we have a variable which we compare with different values

            var season = Season.Spring;

            switch (season)
            //      1
            {
                case Season.Autumn:
                    Console.WriteLine("It's autumn and a beatuiful season.");
                    break;

                case Season.Summer:
                    Console.WriteLine("It's perfect to go to the beach.");
                    break;

                case Season.Winter:
                case Season.Spring:
                    Console.WriteLine("We've got a promotion!");
                    break;
            //  2

                default:
            //  3
                    Console.WriteLine("I don't understand that season!");
                    break;
            }
            /* 1 - switch is evaluating members in the enum (class) season
             * at least 1 case statement must be evaluated, but can have more
             * if this memeber, than execute this code etc..
             * 2 - 2 conditions result in the same code
             * 3 - default: anything else other than the cases evaluated, than execute this code..
             */
        }
    }
}
