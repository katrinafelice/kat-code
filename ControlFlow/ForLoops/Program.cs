﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            //For loop to check for even numbers between 1-10 and display on console

            for (var i = 1; i <= 10; i++)
            {
                if (i % 2 == 0)
                    Console.WriteLine(i);
            }

            for (var i = 10; i >= 1; i--)
            {
                if (i % 2 == 0)
                    Console.WriteLine(i);
            }
        }
    }
}
