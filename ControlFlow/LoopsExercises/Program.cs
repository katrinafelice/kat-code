﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoopsExercises
{
    class Program
    {
        static void Main(string[] args)
        {
            //1- Write a program to count how many numbers between 1 and 100 are divisible by 3 with no remainder. 
            //Display the count on the console.

            //var count = 0;

            //for (var i = 1; i <= 100; i++)
            //{
            //    if (i % 3 == 0)
            //        count++;
            //}

            //Console.WriteLine(count);

            // 2- Write a program and continuously ask the user to enter a number or "ok" to exit. 
            //Calculate the sum of all the previously entered numbers and display it on the console.

            //var count = 0;
            //while (true)
            //{
            //    Console.WriteLine("Enter a number or 'ok' to exit:");
            //    var input = Console.ReadLine();

            //    if (!String.IsNullOrWhiteSpace(input))
            //        count++;

            //    else
            //        break;
            //}

            //Console.WriteLine("You have entered a number {0} times.", count);


            //3- Write a program and ask the user to enter a number. Compute the factorial of the number and print it 
            //on the console. For example, if the user enters 5, the program should calculate 5 x 4 x 3 x 2 x 1 and 
            //display it as 5! = 120.

            //Console.Write("Enter a number: ");
            //var number = Convert.ToInt32(Console.ReadLine());

            //var factorial = 1;
            //for (var i = 1; i <= number; i++)
            //    factorial *= i;

            //Console.WriteLine("{0}! = {1}", number, factorial);

            //4- Write a program that picks a random number between 1 and 10. Give the user 4 chances to guess the number. 
            //If the user guesses the number, display “You won"; otherwise, display “You lost". 
            //(To make sure the program is behaving correctly, you can display the secret number on the console first.)

            //var random = new Random();
            //var secretNumber = random.Next(1, 10);

            //Console.WriteLine("The secret number is {0}.", secretNumber);


            //for (var i = 4; i >= 1; i--)
            //{
            //    Console.WriteLine("Guess a number between 1 to 10.  You have {0} attempt(s) left.", i);
            //    var input = Convert.ToInt32(Console.ReadLine());
            //
            //    if (input == secretNumber)
            //    {
            //        Console.WriteLine("You won a burger!");
            //        break;
            //    }
            //
            //    else
            //    {
            //        Console.WriteLine("You lost.");
            //    }

            //5- Write a program and ask the user to enter a series of numbers separated by comma. 
            //Find the maximum of the numbers and display it on the console. For example, if the user enters 
            //“5, 3, 8, 1, 4", the program should display 8.

            Console.WriteLine("Enter a series of numbers separaed by a comma:");
            var input = Console.ReadLine();

            var numbers = input.Split(',');
            var max = Convert.ToInt32(numbers[0]);

            foreach (var str in numbers)
            {
                var number = Convert.ToInt32(str);
                if (max > number)
                    max = number;
            }

            Console.WriteLine("Max is " + max);


        }




    }
}
