﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            //While loop to check for even numbers between 1-10 and display on console

            var i = 0;
            while (i <= 10)
            {
                if (i % 2 == 0)
                    Console.WriteLine(i);

                i++;
            }

            Console.WriteLine();

            //For loop is typically used when you already know how many times you want to iterate
            //While loop is typically used when you don't know how many times iteration will last
            //While loop isn't as clean in the above example (use for loop)

            //Echo program - user is asked to write their name, if they do, console will echo it,
            //but if user presses enter without writing their name, progam will terminate
            //We don't know how many times the user is going to enter their name, hence how many times
            //to iterate

            while (true)
                //while (true) will execute forever unless a break clause is declared
            {
                Console.Write("Type your name: ");
                var input = Console.ReadLine();

                if (String.IsNullOrWhiteSpace(input))
                    break;

                Console.WriteLine("@Echo: " + input);
                //if (String.IsNullOrWhiteSpace(input)) condition is not me, then Console.WriteLine will execute

            }

            //Another way of writing the above code:

            while (true)
            {
                Console.Write("What's your favourite colour?");
                var input2 = Console.ReadLine();

                if (!String.IsNullOrWhiteSpace(input2))
                    //is String is NOT Null or WhiteSpace i.e if String contains characters
                {
                    Console.WriteLine("@Echo " + input2);
                    continue;
                    //this continue takes the program to the top of the while loop i.e while (true)
                }

                break;
            }

        }
    }
}
