﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomClass
{
    class Program
    {
        static void Main(string[] args)
        {
            //To create an instance of the random class:

            var random = new Random();

            //random.Next - returns a random integer
            //random.NextBytes - takes an array and fills it with random numbers
            //random.NextDouble - returns a random double between 0.0. and 1.1

            for (var i = 0; i < 10; i++)
                Console.WriteLine(random.Next());

            Console.WriteLine();

            var random2 = new Random();
            for (var i = 0; i < 10; i++)
                Console.WriteLine(random2.Next(2));

            //random2.Next(10) - 2nd overload asks for max int for random numbers

            Console.WriteLine();

            var random3 = new Random();
            for (var i = 0; i < 10; i++)
                Console.WriteLine(random3.Next(4,16));

            //random3.Next(3,5) -3rd overload asks for min int and max int for random numbers

            Console.WriteLine();

            Console.WriteLine((int)'a');
            //returns 97 - this is the ASCII number for 'a' i.e character cast as integer

            Console.WriteLine();

            //To create a random sequence of characters and set as a password:

            var random4 = new Random();
            //first create an instance of the Random class

            const int passwordLength = 10;

            var buffer = new char[passwordLength];
            //declare a character array, size is defined by passwordLength

            for (var i = 0; i < passwordLength; i++)
                buffer[i] = (char)('a' + random4.Next(0, 26));
            //this is an expressive way of using ASCII - 'a' = 26, 26 + 0 = a etc.
            //cast the int to a char as buffer array contains characters

            var password = new string(buffer);
            //another overload for declaring a string, that is filled by a character array

            Console.WriteLine(password);
        }
    }
}
