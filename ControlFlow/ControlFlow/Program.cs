﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlFlow
{
    class Program
    {
        static void Main(string[] args)
        {
            //IF/ELSE CONDITIONS: 
            //we have a condition that is evaluate, if it is true than a certain code is executed

            int hour = 10;

            if (hour > 0 && hour < 12)
            {
                Console.WriteLine("It's morning.");
            }
            else if (hour >= 12 && hour < 18)
            {
                Console.WriteLine("It's afternoon.");
            }
            else
                Console.WriteLine("It's evening.");

            //BOOLEAN CONDITIONS

            bool isGoldCustomer = true;
            float price;
            if (isGoldCustomer)
                price = 19.95f;
            else price = 29.95f;
            Console.WriteLine(price);

            //or rewrite same code using conditional, as shortcut:

            float price2 = (isGoldCustomer) ? 19.95f : 29.95f;
            Console.WriteLine(price2);
            
            /*              1               2   3    4  5
             * 1 - condition
             * 2 - is condition true?
             * 3 - if condition is true than price2 will be 19.95, f explicit for float
             * 4 - else
             * 5 - price will be 29.95f
             * 
             */

        }
    }
}
