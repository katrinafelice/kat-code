﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReferenceTypesAndValueTypes
{
    class Program
    {
        static void Main(string[] args)
        {
            //VALUE TYPES (e.g int)
            var a = 10;
            var b = a;
            b++;
            Console.WriteLine(String.Format("a: {0}, b: {1}", a, b));
            // a = 10, b = 11 because integers are value types so a copy of the value is stored in a new location

            //REFERENCE TYPES (e.g class such as array)
            var array1 = new int[3] { 1, 2, 3 };
            var array2 = array1;
            array2[0] = 0;
            Console.WriteLine(string.Format("array1[0]: {0}, array2[0]: {1}", array1[0], array2[0]));
            //in reference types, memory address is copied from heap to stack, value is not copied to a new location
        }
    }
}
