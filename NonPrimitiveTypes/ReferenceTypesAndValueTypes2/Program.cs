﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReferenceTypesAndValueTypes2
{
    //declare a new class
    public class Person
    {
        public int Age;
    }

    class Program
    {
        static void Main(string[] args)
        {
            //VALUE TYPE
            var number = 1;
            Increment(number);
            Console.WriteLine(number);
            //number = 1 because this is a value type when you pass this variable as an argument to the Incremement method
            //a copy of this value is taken and sent to the Increment method
            //number is incremented by 10 and its location is immediately destroyed

            //REFERENCE TYPE
            var person = new Person() { Age = 20 };
            MakeOld(person);
            Console.WriteLine(person.Age);
            /*
             * object that is passed here is not going to be going to be copied, its reference is 
             * going to be copied ie both the person obejct in the main method and the object in the 
             * parameter will both be pointing to the same object on the heap
            */
        }

        public static void Increment(int number)
        {
            number += 10;
        }

        public static void MakeOld(Person person)
        {
            person.Age += 10;
        }
    }
}
