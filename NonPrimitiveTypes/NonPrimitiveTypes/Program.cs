﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NonPrimitiveTypes.Person;
using NonPrimitiveTypes.Math;

namespace NonPrimitiveTypes
{
    }
    class Program
    {
        static void Main(string[] args)
        {
            //To create an object of type person:
            Person John = new Person();
            John.FirstName = "John";
            John.LastName = "Smith";
            John.Introduce();

        //To create an object of type calculator:
        Calculator calculator = new Calculator();
        var result = calculator.Add(1,2);
        Console.WriteLine(result);
        
        
        

        }
    }
 

