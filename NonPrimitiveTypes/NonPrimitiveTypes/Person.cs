﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NonPrimitiveTypes.Person
{
    //Stick to 1 class per file
    //To create a new class, declare at namespace level:

    public class Person
    {

        public string FirstName;
        //this is a field
        public string LastName;

        public void Introduce()
        //this is a method, 'void' means it doesn't return a value
        {
            Console.WriteLine("My name is " + FirstName + " " + LastName);
        }
    }
}
