﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NonPrimitiveTypes.Math
{
    public class Calculator
    {
        public int Add(int a, int b)
        //this says that in the Add method, we are asking for 2x int separted by comma
        {
            return a + b;
            //this says that the result of this method should be the sum of the 2x int
        }
    }
}
