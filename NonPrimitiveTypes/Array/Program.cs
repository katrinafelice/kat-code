﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array
//An array is a data structure used to store a collection of variables of the same type.
//An array is a class, so we declare an instance of the array as an object, therefore memory 
//must be allocated.
{
    class Program
    {
        static void Main(string[] args)
        {
            //To declare an int array:

            var numbers = new int[3];
            int[] numbers2 = new int[3];
            //'numbers' is a cleaner way of declaring an array, 'numbers2' is long
            //[3] specifies the size of this array

            numbers[0] = 1;
            //array is zero indexed and default value for int (if not assigned) is 0

            Console.WriteLine(numbers[0]);
            Console.WriteLine(numbers[1]);
            Console.WriteLine(numbers[2]);

            //To declare a bool array:

            var flags = new bool[3];
            flags[0] = true;
            //default value for bool (if not assigned) is false

            Console.WriteLine(flags[0]);
            Console.WriteLine(flags[1]);
            Console.WriteLine(flags[2]);

            //To declare a string array:

            var names = new string[3] {"Kat", "Gremlin", "Daisy" };
            //object initialization syntax inbetween {} and separated by ,

            Console.WriteLine(names[2]);
        }
    }
}
