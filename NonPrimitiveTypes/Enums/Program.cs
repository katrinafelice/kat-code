﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Declare an enum at namespace level, just as a class.
//Use enus when you have a number of related constants, instead of declaring multiple constants.
//Assign values to fields when declaring (advised), otherwise default value of 0 and increment of +1 to
//subsequent fields.
//internally enums are integers

public enum ShippingMethod
{
    RegularAirMail = 1,
    RegisteredAirMail = 2,
    Express = 3
}

namespace Enums
{
    class Program
    {
        static void Main(string[] args)
        {
            //to return a numeric value corresponding to an enum member
            var method = ShippingMethod.Express;
            Console.WriteLine((int)method);
            //cast the enum to a numeric value

            //to access an enum member using a value input
            var methodId = 3;
            Console.WriteLine((ShippingMethod)methodId);

            //to convert an enum to a string
            //all members of a class have the ToString() method
            Console.WriteLine(method.ToString());

            //Console.WriteLine() automatically converts any object to string, however you can explicitly do so
            //or perhaps you aren't using Console.WriteLine in which case you should use the ToString() method
            Console.WriteLine(method);

            //to convert a string to an enum
            //'Parsing' - converting a string to another type
            var methodName = "Express";
            var shippingMethod = (ShippingMethod)Enum.Parse(typeof(ShippingMethod), methodName);
            /*         7                  6          1    2     3          4              5
             * 1 - access enum class
             * 2 - use parse method
             * 3 - first overload asks for type of
             * 4 - asks for the target type (ShippingMethod)
             * 5 - asks for the string value (string value)
             * 6 - return value for Enum.Parse is an object, so this must be case to the enum in question i.e ShippingMethod
             * 7 - var storing the result of the expression which is an enum
            */




        }
    }
}
