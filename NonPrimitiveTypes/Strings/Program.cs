﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strings
{
    class Program
    {
        static void Main(string[] args)
        {
            var firstName = "Katrina";
            string lastName = "Felice";
            //var or string are the same here, both map to an object of type class in the .NET framework

            String middleName = "Lilly";
            //maps directly to the class (?) so must be accessing using System; namespace

            Int32 i;
            int j;
            //declaring int via class or instance of class - same thing

            //String Concatenation (joining strings together)
            var fullName = firstName + " " + lastName;

            //String.Format (join strings together using static method in String class)
            var myFullName = string.Format("My name is {0} {1}", firstName, lastName);

            //String.Join (join strings using static method in String class)
            var names = new string[3] {"Tom", "Kat", "Sam" };
            //here we've set up a new string array to contain the 3x strings
            var formattedNames = string.Join(",", names);
            //string.Join takes 2 parameters - the first is the separator, the second is the array
            Console.WriteLine(formattedNames);

            //Verbatim strings
            var text = "Hi John\nLook into the following paths\nc:\\folder1\\folder2\nc:\\folder3\\folder4";
            Console.WriteLine(text);
            //the above string is hard to read using /n for new line and \\ for backsplash, instead:

            var cleanText = @"Hi John
Look into the following paths
c:\folder1\folder2
c:\folder3\folder4";
            Console.WriteLine(cleanText);
        }
    }
}
