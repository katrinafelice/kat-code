﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringsExercises
{
    class Program
    {
        static void Main(string[] args)
        {
            /// eg1 Write a program and ask the user to enter a few numbers separated by a hyphen. Work out 
            /// if the numbers are consecutive. For example, if the input is "5-6-7-8-9" or "20-19-18-17-16", 
            /// display a message: "Consecutive"; otherwise, display "Not Consecutive".
            /// </summary>

            Console.WriteLine("Write a few numbers separated by a hyphen, eg 1-2-3-4: ");
            var input = Console.ReadLine();
            var numbers = new List<int>();

            foreach (var str in input.Split('-'))
            {
                numbers.Add(Convert.ToInt32(str));

            }

            var isConsecutive = false;
            numbers.Sort();

            //foreach (var number in numbers)
            //{
            //    Console.WriteLine(number);
            //}

            for (var i = 1; i < numbers.Count; i++)
            {
                if (numbers[i] == numbers[i - 1] + 1)
                    isConsecutive = true;
                else
                    isConsecutive = false;
            }

            var message = isConsecutive ? "Consecutive" : "Not consecutive";
            Console.WriteLine(message);

            //2- Write a program and ask the user to enter a few numbers separated by a hyphen. 
            //If the user simply presses Enter, without supplying an input, exit immediately; 
            //otherwise, check to see if there are duplicates. If so, display "Duplicate" on the console.

            //Console.WriteLine("Enter a few numbers separated by a hyphen e.g '1-2-3-3':");
            //var input = Console.ReadLine();

            //if (String.IsNullOrWhiteSpace(input))
            //    return;

            //var numbers = new List<int>();

            //foreach (var item in input.Split('-'))
            //numbers.Add(Convert.ToInt32(item));

            //var uniqueNumbers = new List<int>();
            //var containsDuplicates = false;

            //foreach (var number in numbers)
            //{
            //    if (!uniqueNumbers.Contains(number))
            //    {
            //        uniqueNumbers.Add(number);
            //    }
            //    else
            //    {
            //        containsDuplicates = true;
            //    }
            //}

            //if (containsDuplicates == true)
            //    Console.WriteLine("Duplicate");

            //3- Write a program and ask the user to enter a time value in the 24-hour time format (e.g. 19:00). 
            //A valid time should be between 00:00 and 23:59. If the time is valid, display "Ok"; otherwise, display 
            //"Invalid Time". If the user doesn't provide any values, consider it as invalid time. 

            //Console.WriteLine("Enter a time in 24 hour format, eg '12:30'");
            //var input = Console.ReadLine();

            //if (String.IsNullOrWhiteSpace(input))
            //{
            //    Console.WriteLine("invalid");
            //    return;
            //}

            //var inputs = input.Split(':');

            //if (inputs.Length != 2)
            //{
            //    Console.WriteLine("Invalid Time");
            //    return;
            //}

            //var hour = Convert.ToInt32(inputs[0]);
            //var mins = Convert.ToInt32(inputs[1]);

            //if (hour >= 00 && hour <= 24 && mins >= 00 && mins <= 59)
            //{
            //    Console.WriteLine("valid");
            //}
            //else
            //{
            //    Console.WriteLine("invalid");
            //}

            //4- Write a program and ask the user to enter a few words separated by a space. 
            //Use the words to create a variable name with PascalCase. For example, if the user types: 
            //"number of students", display "NumberOfStudents". Make sure that the program is not dependent on the input. 
            //So, if the user types "NUMBER OF STUDENTS", the program should still display "NumberOfStudents".

            //Console.WriteLine("Enter a few words separated by a space, eg 'yellow octopus for breakfast': ");
            //var input = Console.ReadLine();

            //if (String.IsNullOrWhiteSpace(input))
            //{
            //    Console.WriteLine("Silly, you didn't write anything..");
            //    return;
            //}

            //var words = input.ToLower().Split(' ');

            //foreach (var word in words)
            //{
            //    var wordWithPascalCase = char.ToUpper(word[0]) + word.Substring(1);
            //    Console.Write(wordWithPascalCase);

            //}

            //5- Write a program and ask the user to enter an English word. Count the number of vowels (a, e, o, u, i) in the word. 
            //So, if the user enters "inadequate", the program should display 6 on the console.

            //char[] vowels = { 'a', 'e', 'i', 'o', 'u' };

            //Console.WriteLine("Enter an English word: ");
            //var input = Console.ReadLine().ToLower();

            //var count = 0;
            //foreach (var letter in (input))
            //{
            //    if (vowels.Contains(letter))
            //        count++;
            //}
            //Console.WriteLine("Number of vowels: " + count++);
        }
    }
}
