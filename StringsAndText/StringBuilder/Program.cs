﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stringBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            //String builder is more efficient than String class when it comes to manipulating strings because
            //it doesn't not allocate new memory for each new manipulation however string builder does not
            //have searching methods

            var builder = new StringBuilder();
            //make sure using System.Text; is included above namespace

            //Append
            builder.Append('-', 10);
            builder.AppendLine();
            builder.Append("Header");
            builder.AppendLine();
            builder.Append('-', 10);

            Console.WriteLine(builder);

            //Replace
            builder.Replace('-', '.');
            Console.WriteLine(builder);

            //Remove
            builder.Remove(0, 10);
            Console.WriteLine(builder);

            //Insert
            builder.Insert(0, new string('*', 10));
            Console.WriteLine(builder);

            //Declare a StringBuilder object with a string as an object initializer
            var text = new StringBuilder("Hello World!");
            text.Remove(0, 5);
            Console.WriteLine(text);

            //Indexing
            Console.WriteLine(text[2]);

            //Chain methods together because return value is a String Builder (not void):

            text
                .AppendLine()
                .Append("Hello sunshine!")
                .Replace('l', 'L');

            Console.WriteLine(text);
        }
    }
}
