﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringsAndText
{
    class Program
    {
        static void Main(string[] args)
        {

            //string keyword maps to String class in .NET Framework
            //strings are immutable which means they cannot be changed once declared

            /*Methods - Formatting
             * ToLower()    "hello world"
             * ToUpper()    "HELLO WORLD"
             * Trim()       removes white space (eg from a user input)
             */

            /*Methods - Searching
             * IndexOf('a')
             * LastIndexOf("Hello")
             * //each have different overloads enabling you to search for a character or a string
             */

            /*Methods - Substring
             * Substring(startIndex)
             * Substring(startIndex, length)
             */

            /*Methods - Replacing
             * Replacing('a'. '!')
             * Replace ("mosh", "moshfegh")
             */

            /*Methods - Null checking
             * String.IsNullOrEmpty(str)
             * String.IsNullorWhiteSpace(str)
             */

            /*Methods - Splitting
             * str.Split(' ')       - this splits a string with white space separating the words into an array
             * off different strings
             */ 

            /*Converting Strings to Numbers
             * string s = "1234";
             * int i = int.Parse(s); or
             * int j = Convert.ToInt32(s);
             * //Convert.ToInt32 method is safer because it coverts null or white space to default int '0'
             * whereas Parse method returns an exception
             */

            /*Converting Numbers to String
             * int i = 1234;
             * string s = i.ToString();         - "1234"
             * string t = i.ToString("C")       - "$1,234.00" - C/c formats for currency with dollar sign 
             * and 2 decimal points
             * string t = i.ToString("C0");     - "$1,234"      - C0 formats currency with dollar sign 
             * and no decimal points   
             * 
             * Format Specifier     Description         Example
             * c or C               Currency            123456 (C) -> $123,456
             * d or D               Decimal             1234 (D6) -> 001234
             * e or E               Exponential         1052.0329112756 (E) -> 1.052033E+003
             * f or F               Fixed Point         1234.567 (F1) -> 1234.5
             * x or X               Hexadecimal         255 (X) -> ff
             */

       
        }
    }
}
