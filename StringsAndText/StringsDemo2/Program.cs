﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringsDemo2
{
    class Program
    {
        static void Main(string[] args)
        {
            var fullName = "Katrina Felice ";
            Console.WriteLine("Effect of Trim: '{0}'", fullName.Trim());
            Console.WriteLine("Effect of ToUpper: " + fullName.ToUpper());

            var names = fullName.Split(' ');
            Console.WriteLine("First name: " + names[0]);
            Console.WriteLine("Last name: " + names[1]);

            var sillyName = "Kitty/Too-Too";
            var index = sillyName.IndexOf('/');
            var firstName = sillyName.Substring(0, index);
            var lastName = sillyName.Substring(index + 1);

            Console.WriteLine("Silly first name: " + firstName);
            Console.WriteLine("Silly last name: " + lastName);

            Console.WriteLine(sillyName.Replace("Kitty", "Birdie"));
            Console.WriteLine(sillyName.Replace("Too", "Poo"));
            Console.WriteLine(fullName.Replace(" ", ""));

            Console.WriteLine();
           
            

            

            var str = "52";
            var magicNumber = Convert.ToByte(str);
            Console.WriteLine("You will recieve a magical surprise when you turn " + magicNumber + " years old");

            float price = 18.10f;
            var cost = price.ToString("C0");
            Console.WriteLine(cost);

            while (true)
            {
                Console.WriteLine("Hi Kat, say something please: ");
                var input = Console.ReadLine();
                if (String.IsNullOrWhiteSpace(input))
                    Console.WriteLine("Naughty Kat, you didn't say anything!");
                else
                    Console.WriteLine("I will contemplate this, thanks Kat..");

            }
        }
    }
}
