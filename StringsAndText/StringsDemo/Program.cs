﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringsDemo
{
    class Program
    {
        static void Main(string[] args)
        {

            //Trim & ToUpper

            var fullName = "Katrina Felice ";
            Console.WriteLine("Effect of trim: '{0}'", fullName.Trim());
            //.Trim removes white space at beginning and/or end of string
            Console.WriteLine("Effect of ToUpper: '{0}'", fullName.Trim().ToUpper());
            //these return new srings, do NOT modify existing string because a string is immutable

            //IndexOf (white space) and Substrings to separate strings

            var index = fullName.IndexOf(' ');
            var firstName = fullName.Substring(0, index);
            //this overload requireds first char (index 0) and where to end substring @ white space ' ' 
            //(named var index)
            var lastName = fullName.Substring(index + 1);
            //this overload only requires where to start substring
            Console.WriteLine("FirstName: " + firstName);
            Console.WriteLine("LastName: " + lastName);

            //Split

            var names = fullName.Split(' ');
            //creates an array of strings
            Console.WriteLine("First name: " + names[0]);
            Console.WriteLine("Last name: " + names[1]);

            //Replace
            Console.WriteLine("The Russian equivalent of my name is " + fullName.Replace("Katrina", "Katrushka"));
            Console.WriteLine("I like to be called: " + fullName.Replace("a", "A"));
            Console.WriteLine("Replace white space with null space: " + fullName.Replace(" ", ""));
            //This is getting rid of spaces kind of like splitting earlier

            //Validate user input as valid or invalid depending on whether null or empty or white space

            if (String.IsNullOrEmpty(""))
                Console.WriteLine("invalid");

            if (String.IsNullOrEmpty(" "))
                Console.WriteLine("invalid");
            else
                Console.WriteLine("valid");
            //this returns valid because technically white space is still a character, therefore use:

            if (String.IsNullOrWhiteSpace(" "))
                Console.WriteLine("invalid");

            //Convert string to integer

            var str = "25";
            var age = Convert.ToByte(str);
            //Convert.ToByte because no one can be older than 250 so 1 byte of storage is enough
            Console.WriteLine(age);

            //Convert a number to a string

            float price = 29.95f;
            Console.WriteLine(price.ToString());
            //every object in .NET has a ToString method

            Console.WriteLine(price.ToString("C"));
            //formats string as currency

            Console.WriteLine(price.ToString("C0"));
            //formats string as currency, rounding to zero decimal points
        }
    }
}
