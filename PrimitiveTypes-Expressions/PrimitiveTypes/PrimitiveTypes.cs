﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimitiveTypes
{
    class PrimitiveTypes
    {
        static void Main(string[] args)
        {
            /*
             * C# type  -   .NET type   -   Bytes   -   Range
             * 
             * byte     -   Byte        -   1       -   0 to 255
             * short    -   Int16       -   2       -   -32,768 to 32,767
             * int      -   Int32       -   4       -   -2.1B to 2.1B
             * long     -   Int4        -   8       -   ...
             * float    -   Single      -   4       -   ...
             * double   -   Double      -   8       -   ...
             * decimal  -   Decimal     -   16      -   ...
             * char     -   Char        -   2       -   Unicode Characters
             * bool     -   Boolean     -   1       -   True / False
             * 
             * These types have an equivalent type in .NET Framework.  So when you compile your application
             * the compiler maps your types to the underlying type in .NET Framework.
             *  
             */
             
        }
    }
}
