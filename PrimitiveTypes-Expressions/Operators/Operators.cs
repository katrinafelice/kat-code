﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operators
{
    class Operators
    {
        static void Main(string[] args)
        {
            /*
             * In C# we have 4 types of operators:
             * 1.   Arithmetic: used for computations
             * 2.   Comparison: used for comparing values in boolean expressions
             * ..   Assigment Operators: to assign values
             * 3.   Logical: represents logical AND, OR and NOT
             * 4.   Bitwise: represents bitwise AND, OR and NOT
             *-----------------------------------------------------------------------
             * 
             * 1.   Arithmetic: use for computations using numbers
             * Description  -   Operator    -   Example
             * Add          -   +           -   a+b
             * Subtract     -   -           -   a-b
             * Multiply     -   *           -   a*b
             * Divide       -   /           -   a/b
             * Remainder    -   %           -   a%b (remainder of division)
             * Increment    -   ++          -   a++ (a=a+1)
             * Decrement    -   --          -   a-- (a=a-1)
             * 
             *  int a = 5;
             *  int b = ++a; // b = 6, a = 6 PREFIX INCREMENT
             *  int c = a++; // c = 6, a = 7 POSTFIX INCREMENT
             *  at post increment you get the value before you incremented
             *  at pre-increment you get the value after you incremented
             *  pre-increment is a tiny bit faster because you don't have to store the previous value
             * 
             * 
             * 2.   Comparison: result is always boolean true/false
             * Description  -   Operator    -   Example
             * Equal        -   ==          -   a==b
             * Not equal    -   !=          -   a!=b
             * Greater than -   >           -   a>b
             * Greater than / equal to - >= -   a>=b
             * Less than    -   <           -   a<b
             * Less than or equal to - <=   -   a<=b
             * 
             * 
             *..   Assigment Operators: to assign values
             * Description  -   Operator    -   Example -   Same as
             * Assignment   -   =           -   a=1
             * Addition     -   +=          -   a+=3    -   a=a+3
             * Subtraction  -   -=          -   a-=3    -   a=a-3
             * Multiplication - *=          -   a*=3    -   a=a*3
             * Division     -   /=          -   a/=3    -   a=a/3
             * 
             * 
             * 3.   Logical Operators: used in boolean expressions, often used in conditional statements
             * Description  -   Operator    -   Example
             * Logical AND          -   &&          -   a&&b
             * Logical OR           -   ||          -   a||b
             * Logical NOT          -   !           -   !a
             * 
             * 
             * 4.   Bitwise Operators: low level programming like windows API, sockets, encryption
             * Description  -   Operator    -   Example
             * Bitwise AND          -   &           -   a&b
             * Bitwise OR           -   |           -   a|b
             * 
             * 
             */
        }
    }
}
