﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateTimeAndTimeSpan
{
    class Program
    {
        static void Main(string[] args)
        {
            //DateTime is a structure defined in System namespace     

            var dateTime = new DateTime(2015, 1, 1);
            var now = DateTime.Now;
            var today = DateTime.Today;

            Console.WriteLine("Hour: " + now.Hour);
            Console.WriteLine("Minute: " + now.Minute);
            Console.WriteLine(dateTime);

            //DateTime objects in C# are immutable, once created, cannot be changed but can be modified
            //with the .Add method

            var tomorrow = now.AddDays(1);
            var yesterday = now.AddDays(-1);
            //negative to subtract

            //Methods to convert DateTime to a string

            Console.WriteLine(now.ToLongDateString());
            Console.WriteLine(now.ToShortDateString());
            Console.WriteLine(now.ToLongTimeString());
            Console.WriteLine(now.ToShortDateString());

            //To display date and time in string with a format specifier
            Console.WriteLine(now.ToString("yyyy-mm-dd HH:mm"));

            //Google c# datetime format specifier for whole list of specifiers


        }
    }
}
