﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace timeSpan
{
    class Program
    {
        static void Main(string[] args)
        {

        //TimeSpan represents a length of time
        //Creating timeSpan objects

        var timeSpan = new TimeSpan(1, 2, 3);
        //overload takes hr, min, sec
        var timeSpan1 = new TimeSpan(1, 0, 0);
        // not clear in overload what each int represents, more readable way is:

        var TimeSpan2 = TimeSpan.FromHours(1);

        //the difference between 2 dateTimes will give you the timeSpan:

        var start = DateTime.Now;
        var end = DateTime.Now.AddMinutes(2);

        var duration = end - start;

        Console.WriteLine("Duration: " + duration);

            //Properties (pairs of properites, eg timeSpan.Minutes & timeSpan.TotalMinutes etc.

            Console.WriteLine(timeSpan.Minutes);
            //timeSpan.Minutes returns minute component of your timeSpan object

            Console.WriteLine(timeSpan.TotalMinutes);
            //timeSpan.TotalMinutes converts the total timeSpan into minutes


            //Add
            //timeSpan is immutable, just like DateTime, but you can modify with different .Add() and .Subtract() 
            //which creates a new object

            Console.WriteLine("Add Example: " + timeSpan.Add(TimeSpan.FromMinutes(8)));
            Console.WriteLine("Subtract Example: " + timeSpan.Subtract(TimeSpan.FromMinutes(2)));
            Console.WriteLine(timeSpan.ToString());

            //Convert a TimeSpan to a String
            Console.WriteLine("To String " + timeSpan.ToString());

            //Parse (convert from a string)
            Console.WriteLine("Parse: " + TimeSpan.Parse("01:02:03"));


        }
    }
}
