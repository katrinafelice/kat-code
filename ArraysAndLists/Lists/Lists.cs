﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lists
{
    class Lists
    {
        static void Main(string[] args)
        {
            //array - fixed size
            //list - dynamic size, data structure for storing data of same types; used when we don't know
            //ahead of time how large the data structure will be

            //To declare a list:
            var numbers1 = new List<int>();
            //<...> - arrow bracks means it is a generic
            //int - you can create a list of any type

            //Use object initialization if you know what data you want to store in the list
            var numbers2 = new List<int>() { 1, 2, 3, 4 };

            //Useful methods
            //Add()
            //AddRange() - could be to add a list or an array
            //Remove()
            //RemoveAt()
            //IndexOf()
            //Contains()
            //Count

            //Add and AddRange

            var numbers = new List<int>() {1,2,3,4 };
            numbers.Add(1);
            numbers.AddRange(new int[3] { 5, 6, 7 });
            //in overload for .AddRange, IEnumerable - I stands for interface, use an array or list
            numbers.Add(90);
            numbers.AddRange(numbers2);

            //IndexOf()
            //numbers.IndexOf(int): returns index of int, if it doesn't exist, returns -1

            Console.WriteLine();
            Console.WriteLine("Index of 1: " + numbers.IndexOf(1));
            //Returns first index of that int

            numbers.Add(1);
            Console.WriteLine();
            Console.WriteLine("Index of 1: " + numbers.LastIndexOf(1));
            //Returns last index of that int

            //Count: property that returns the number of objects in a list

            foreach (var n in numbers)
                Console.WriteLine(n);

            Console.WriteLine();

            Console.Write("Count of numbers is: " + numbers.Count);

            //Remove()

            Console.WriteLine();

            numbers.Remove(1);
            // Removes only the first int that matches the argument

            foreach (var n in numbers)
                Console.WriteLine(n);

            Console.WriteLine();

            //To remove each int that matches an argument within a list, this returns an error because
            // in C# we are not allowed to modify a collection inside a foreach loop:
            //foreach (var n in numbers)
            // if (n == 1)
            //{
            //numbers.Remove(n);
            //this modifies the collection
            //}
            //foreach (var n in numbers)
            //Console.WriteLine(n);

            Console.WriteLine("Attempt to remove all int = 1: ");

            for (var i = 0; i <= numbers.Count; i++)
            {
                if (numbers[i] == 1)
                    numbers.Remove(numbers[i]);
            }

            foreach (var n in numbers)
            Console.WriteLine(n);


            //Clear() - this method clears all elements from the list

            numbers.Clear();
            Console.WriteLine("Count of numbers: " + numbers.Count);
        }
    }
}
