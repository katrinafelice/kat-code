﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays
{
    class Arrays
    {
        static void Main(string[] args)
        {
            //To declare a rectangular 2D array:

            var numbers = new[] { 3, 6, 9, 14, 6 };

            //Length
            Console.WriteLine("Length: " + numbers.Length);

            //IndexOf()
            var index = Array.IndexOf(numbers, 9);
            Console.WriteLine("Index of 9: " + index);

            //Clear()

            Array.Clear(numbers, 0, 2);
            //this overload: array identifier, index to start clear from, length to clear from index

            Console.WriteLine("Efect of Clear()");
            foreach (var n in numbers)
                Console.WriteLine(n);

            // clear an array of int - returns 0
            // clear an array of bool - returns fals
            // clear an array of string - returns null

            //Copy()
            int[] anotherArray = new int[3];
            Array.Copy(numbers, anotherArray, 3);
            //this overload: source array, destination array, length to copy from source array

            Console.WriteLine("Effect of Copy()");
            foreach (var n in anotherArray)
                Console.WriteLine(n);

            //Sort() - sorts from smallest to greatest int
            Array.Sort(numbers);

            Console.WriteLine("Effect of Sort()");
            foreach (var n in numbers)
                Console.WriteLine(n);

            //Reverse()
            Array.Reverse(numbers);

            Console.WriteLine("Effect of Reverse()");
            foreach (var n in numbers)
                Console.WriteLine(n);

            //Array.member - STATIC (method/field accessible via Class) e.g Array.IndexOf(numbers)
            //Object.member - INSTANCE (method/field accessible via instance of Class) e.g numbers.Length
            // if unsure about whether method/field is accessible via class or instance, search google for
            // it and on MSDN if S visible near method/field than it is static, hence accessible via Class
            
        }
    }
}
