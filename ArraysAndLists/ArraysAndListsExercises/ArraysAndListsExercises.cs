﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysAndListsExercises
{
    class ArraysAndListsExercises
    {
        static void Main(string[] args)
        {
            /*1- When you post a message on Facebook, depending on the number of people who like your post, 
             * Facebook displays different information.
                If no one likes your post, it doesn't display anything.
                If only one person likes your post, it displays: [Friend's Name] likes your post.
                If two people like your post, it displays: [Friend 1] and [Friend 2] like your post.
                If more than two people like your post, it displays: [Friend 1], [Friend 2] and 
                [Number of Other People] others like your post.

            Write a program and continuously ask the user to enter different names, until the user 
            presses Enter (without supplying a name). Depending on the number of names provided, 
            display a message based on the above pattern.
             */

            //var friendsList = new List<string>();
            //while (true)
            //{
            //   Console.WriteLine("Please enter a name of someone who likes you: ");
            //    var input = Console.ReadLine();
            //    if (!String.IsNullOrEmpty(input))
            //    {
            //        friendsList.Add(input);
            //   }
            //    else
            //        break;

            //}

            //foreach (var friend in friendsList)
            //Console.WriteLine("Test/ All friends in friendList: " +  friend);

            //if (friendsList.Count == 0)
            //{
            //    Console.WriteLine("No one likes you :( ");
            //}
            //else if (friendsList.Count == 1)
            //{
            //    Console.WriteLine("{0} likes you.", friendsList[0]);
            //}
            //else if (friendsList.Count == 2)
            //{
            //    Console.WriteLine("{0} and {1} like you.", friendsList[0], friendsList[1]);
            //}
            //else if (friendsList.Count > 2)
            //{
            //    Console.WriteLine("{0}, {1} and {2} others like you.", friendsList[0], friendsList[1], friendsList.Count-2);
            //}

            //2- Write a program and ask the user to enter their name. Use an array to reverse the name and then store 
            //the result in a new string. Display the reversed name on the console.

            //Console.WriteLine("What is your name?");
            //var name = Console.ReadLine();

            //var array = new char[name.Length];
            //for (var i = name.Length; i > 0; i--)
            //    array[name.Length - i] = name[i - 1];

            //var reversed = new string(array);
            //Console.WriteLine(reversed);

            //3- Write a program and ask the user to enter 5 numbers. If a number has been previously entered, 
            //display an error message and ask the user to re-try. Once the user successfully enters 5 unique numbers, 
            //sort them and display the result on the console.

            //var numbers = new List <int>();

            //while (numbers.Count < 5)
            //{
            //    Console.WriteLine("Enter a number: ");
            //    var input = Convert.ToInt32(Console.ReadLine());

            //    if (!numbers.Contains(input))
            //    {
            //        numbers.Add(input);
            //    }
            //    else
            //    {
            //        Console.WriteLine("Oops, you've entered that number before, try again: ");
            //        continue;
            //    }
            //}

            //numbers.Sort();
            //Console.WriteLine();
            //Console.WriteLine("The numbers are: ");
            //foreach (var number in numbers)
            //{
            //    Console.WriteLine(number);

            //}

            //4- Write a program and ask the user to continuously enter a number or type "Quit" to exit. 
            //The list of numbers may include duplicates. Display the unique numbers that the user has entered.

            //var numbers = new List<int>();

            //while (true)
            //{
            //    Console.WriteLine("Enter a number or type 'Quit' to exit");
            //    var input = Console.ReadLine();

            //    if (input.ToLower() == "quit")
            //        break;
            //    else
            //        numbers.Add(Convert.ToInt32(input));

            //}

            //var uniqueNumbers = new List<int>();

            //foreach (var number in numbers)
            //    if (!uniqueNumbers.Contains(number))
            //        uniqueNumbers.Add(number);

            //Console.WriteLine("The unique numbers are: ");
            //foreach (var number in uniqueNumbers)
            //    Console.WriteLine(number);

            //5- Write a program and ask the user to supply a list of comma separated numbers (e.g 5, 1, 9, 2, 10). 
            //If the list is empty or includes less than 5 numbers, display "Invalid List" and ask the user to re-try; 
            //otherwise, display the 3 smallest numbers in the list.

            string[] elements;
            while (true)
            {
                Console.WriteLine("Enter a list of numbers separated by a comma: ");
                var input = Console.ReadLine();
                input.Split(',');


                if (!string.IsNullOrEmpty(input))
                {
                    elements = input.Split(',');
                    if (elements.Length <= 5)
                        Console.WriteLine("Oops! That's an invalid list, try again my friend!");
                    break;

                }
            }

            var numbers = new List < int > ();
            foreach (var number in elements)
            {
                numbers.Add(Convert.ToInt32(number));
            }

            var smallests = new List<int>();
            while (smallests.Count < 3)
            {
                // Assume the first number is the smallest
                var min = numbers[0];
                foreach (var number in numbers)
                {
                    if (number < min)
                        min = number;
                }
                smallests.Add(min);

                numbers.Remove(min);
            }

            Console.WriteLine("The 3 smallest numbers are: ");
            foreach (var number in smallests)
                Console.WriteLine(number);






        }
    }
}
