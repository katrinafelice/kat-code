﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysAndLists
{
    class ArraysOverview
    {
        static void Main(string[] args)
        {
            /*Array - represents a fixed number of variables of a paritcular type.
             * can be single dimension or multi dimension
             * 
             * Single dimension: 0 1 2 3 4
             * 
             * Multi dimension (like a matrix) - rectangular:     
             * 0 1 2 3 4
             * 0 1 2 3 4
             * 0 1 2 3 4
             * 
             * Multi dimension - jagged (array of arrays):
             * 0 1 2 3
             * 0 1 2 3 4
             * 0 1 2
            */

            //Declare a rectangular 2D array (of 3 rows and 5 columns):
            var array2D = new int[3, 5]
            {
                { 1, 2, 3, 4, 5},
                { 6, 7, 8, 9, 10},
                { 11, 12, 13, 14, 15}
            };

            //To access a value from this array:

            var element = array2D[2,1];
            Console.WriteLine("Kat thinks this should be 12: " + element);

            //To declare a rectangular 3D array (has 3 properties or dimensions):

            var array3D = new int[3, 5, 4];

            //To declare a jagged array:

            var jaggedArray = new int[3][];
            // [3] means that jaggedArray contains 3 rows, [] means that each row will be initialized below
            jaggedArray[0] = new int[4] {0, 1, 2, 3 };
            jaggedArray[1] = new int[5] {0, 1, 2, 3, 4 };
            jaggedArray[2] = new int[3] {0, 1, 2 };

            //To access an element in this array:

            var elementInJaggedArray = jaggedArray[1][1];
            Console.WriteLine("Kat thinks this should be 1: " + elementInJaggedArray);

            //Syntax Summary
            // Jagged array:        var array = new int [3][];
            // Rectangular array:   var array = new int [3,5];
        
        }
    }
}
